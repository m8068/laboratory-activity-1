import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.*;
import java.util.List;

public class Main {
    List<Show> showsList = new ArrayList<>();
    Show[] filteredShows;
    Show[] showsArray;

    public static void main(String[] args) {
        new Main();
    }

    JFrame mainFrame;
    JPanel mainPanel, headingPanel, searchPanel, filterPanel, titlesPanel;
    JMenuBar menuBar;
    JMenu menu;
    JLabel headingLabel, subHeadingLabel;
    JTextField titleTextField;
    JComboBox genreComboBox, releaseComboBox, ratingComboBox, durationComboBox, typeComboBox;
    JTable table;
    String[] columnNames = new String[]{"Title", "Rating", "Duration", "Date Added"};

    // Stores the list of genres, release years, ratings, durations, and types, respectively
    String[] genres, releaseYears, ratings, durations, types;

    public Main() {
        showsList = readFile(new File("res/netflix_titles.csv"));
        mainFrame = new JFrame("CS 221 Laboratory Activity 1");
        mainPanel = new JPanel();

        // Menu Bar
        menuBar = new JMenuBar();
        menu = new JMenu("About");
        menu.addMenuListener(new MenuListener() {
            public void menuSelected(MenuEvent e) {
                JOptionPane.showMessageDialog(mainFrame, "Carretero, Stephanie Ann\nDollente, Gwynrick Vimer\nEstangki, Jethro\nMilo, Maynard James", "Members", JOptionPane.INFORMATION_MESSAGE);
                menu.setSelected(false);
            }

            public void menuDeselected(MenuEvent e) {

            }

            public void menuCanceled(MenuEvent e) {
                menu.setSelected(false);
            }
        });
        menuBar.add(menu);

        Font headingFont = new Font("Comic Sans", Font.BOLD, 32);
        Font subHeadingFont = new Font("Comic Sans", Font.PLAIN, 18);

        headingLabel = new JLabel("Netflix Releases 2021");
        headingLabel.setFont(headingFont);
        headingLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        subHeadingLabel = new JLabel("(July - September)");
        subHeadingLabel.setFont(subHeadingFont);
        subHeadingLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        headingPanel = new JPanel();
        headingPanel.setLayout(new BoxLayout(headingPanel, BoxLayout.Y_AXIS));
        headingPanel.add(headingLabel);
        headingPanel.add(subHeadingLabel);

        mainPanel.add(headingPanel);

        titleTextField = new HintTextField("Search Title");
        titleTextField.setPreferredSize(new Dimension(640, 40));
        titleTextField.setFont(subHeadingFont);
        titleTextField.addActionListener(new FilterHandler());
        searchPanel = new JPanel();
        searchPanel.add(titleTextField);
        mainPanel.add(searchPanel);

        filterPanel = new JPanel();

        durations = new String[]{"Duration", "< 60 minutes", "60 - 120 minutes", "> 120 minutes", "1 season", "2 seasons", "3 seasons", "4 seasons", "> 4 seasons"};
        genreComboBox = new JComboBox(genres);
        genreComboBox.addActionListener(new FilterHandler());
        releaseComboBox = new JComboBox(releaseYears);
        releaseComboBox.addActionListener(new FilterHandler());
        ratingComboBox = new JComboBox(ratings);
        ratingComboBox.addActionListener(new FilterHandler());
        durationComboBox = new JComboBox(durations);
        durationComboBox.addActionListener(new FilterHandler());

        filterPanel.add(genreComboBox);
        filterPanel.add(releaseComboBox);
        filterPanel.add(ratingComboBox);
        filterPanel.add(durationComboBox);
        mainPanel.add(filterPanel);

        types = new String[]{"TV Show/Movie", "TV Show", "Movie"};
        typeComboBox = new JComboBox(types);
        typeComboBox.addActionListener(new FilterHandler());
        mainPanel.add(typeComboBox);

        table = createTable(convertToObjectArray(showsList.toArray(new Show[showsList.size()])), columnNames);
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new Dimension(640, 480));
        JTableHeader header = table.getTableHeader();
        header.setReorderingAllowed(false);
        header.addMouseListener(new TableHeaderMouseListener());
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 && table.getSelectedColumn() == 0) {
                    findByTitle(table.getValueAt(table.getSelectedRow(), 0).toString()).showFrame();
                }

            }
        });
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        titlesPanel = new JPanel();
        titlesPanel.add(scrollPane);
        mainPanel.add(titlesPanel);
        mainFrame.setJMenuBar(menuBar);
        mainFrame.add(mainPanel);
        mainFrame.setSize(720, 720);
        mainFrame.setResizable(false);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    private List<Show> readFile(File file) {
        try {
            Scanner fileReader = new Scanner(file);
            fileReader.nextLine();

            List<String> genresList = new ArrayList<String>(), releaseYearsList = new ArrayList<String>(), ratingsList = new ArrayList<String>(), castList = new ArrayList<String>();
            while (fileReader.hasNext()) {
                String[] line = fileReader.nextLine().split(",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");
                String type = line[0];
                String title = line[1].trim().replaceAll("^\"|\"$", "");
                String director = line[2].trim().replaceAll("^\"|\"$", "");
                String rating = line[7];
                ratingsList.add(rating);

                int releaseYear = Integer.parseInt(line[6]);
                releaseYearsList.add(String.valueOf(releaseYear));
                int duration = Integer.parseInt(line[8].split(" ")[0]);
                String durationType = line[8].split(" ")[1];
                String date = line[5].trim().replaceAll("^\"|\"$", "");

                List<String> cast = Arrays.asList(line[3].replaceAll("^\"|\"$", "").split(","));
                List<String> genres = new ArrayList<>();
                for (String genre : Arrays.asList(line[9].split(","))) {
                    genre = genre.trim().replaceAll("^\"|\"$", "");
                    genres.add(genre);
                    genresList.add(genre);
                }
                List<String> country = Arrays.asList(line[4].replaceAll("^\"|\"$", "").split(","));
                //add movies/tv shows to the list
                showsList.add(new Show(title, director, type, cast, country, date, releaseYear, rating, duration, durationType, genres));
            }
            genresList.sort(Comparator.naturalOrder());
            genresList.add(0, "Genre");
            genres = setToArray(new LinkedHashSet<String>(genresList));

            releaseYearsList.sort(Comparator.naturalOrder());
            releaseYearsList.add(0, "Release Year");
            releaseYears = setToArray(new LinkedHashSet<String>(releaseYearsList));

            ratingsList.sort(Comparator.naturalOrder());
            ratingsList.add(0, "Rating");
            ratings = setToArray(new LinkedHashSet<String>(ratingsList));
            showsArray = showsList.toArray(new Show[showsList.size()]);
            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return showsList;
    }

    private String[] setToArray(Set<String> set) {
        List<String> list = new ArrayList<>();
        for (String string : set) {
            list.add(string);
        }
        return list.toArray(String[]::new);
    }

    private Object[][] convertToObjectArray(Show[] shows) {
        int length = shows.length;
        Object[][] data = new Object[length][1];
        for (int i = 0; i < length; i++) {
            // title, rating, duration
            data[i] = new Object[]{shows[i].title, shows[i].rating, shows[i].duration + " " + shows[i].durationType, shows[i].dateAdded};
        }
        return data;
    }

    private Show[] filterByType(Show[] shows, String type) {
        return Arrays.stream(shows).filter(show -> checkStringForMatches(show.type, type)).toArray(Show[]::new);
    }

    private Show[] filterByGenre(Show[] shows, String genre) {
        return Arrays.stream(shows).filter(show -> {
            if (genre.equals("")) {
                return true;
            }
            return show.genres.contains(genre);
        }).toArray(Show[]::new);
    }

    private Show[] filterByRating(Show[] shows, String rating) {
        return Arrays.stream(shows).filter(show ->  {
            if (rating.equalsIgnoreCase("")) {
                return true;
            }
            return show.rating.equalsIgnoreCase(rating);

        }).toArray(Show[]::new);
    }

    private Show[] filterByReleaseYear(Show[] shows, int releaseYear) {
        return Arrays.stream(shows).filter(show -> {
            if (releaseYear == 0) {
                return true;
            }
            return show.releaseYear == releaseYear;
        }).toArray(Show[]::new);
    }

    private Show[] filterByDuration(Show[] shows, int duration) {
        return Arrays.stream(shows).filter(show -> {
            if (duration == 0) {
                return true;
            }
            if (duration >= 1 && duration <= 3) {
                if (show.durationType.equalsIgnoreCase("min")) {
                    switch (duration) {
                        case 1:
                            return show.duration < 60;
                        case 2:
                            return show.duration >= 60 && show.duration <= 120;
                        case 3:
                            return show.duration > 120;
                    }
                }
            } else {
                if (show.durationType.equalsIgnoreCase("seasons") || show.durationType.equalsIgnoreCase("season")){
                    switch (duration) {
                        case 4:
                            return show.duration == 1;
                        case 5:
                            return show.duration == 2;
                        case 6:
                            return show.duration == 3;
                        case 7:
                            return show.duration == 4;
                        case 8:
                            return show.duration > 4;
                    }
                }
            }
            return false;
        }).toArray(Show[]::new);
    }

    private Show[] filterByTitle(Show[] shows, String title) {
        return Arrays.stream(shows).filter(show -> checkStringForMatches(show.title, title)).toArray(Show[]::new);
    }

    private boolean checkStringForMatches(String word, String substring) {
        return word.toLowerCase().contains(substring.toLowerCase());
    }

    public Show[] sortByReleaseYearDesc(Show[] listofShows) {
        for (int i = 0; i < listofShows.length; i++) {
            for (int j = i + 1; j < listofShows.length; j++) {
                if (listofShows[i].getReleaseYear() < (listofShows[j].getReleaseYear())) {
                    Show temp = listofShows[i];
                    listofShows[i] = listofShows[j];
                    listofShows[j] = temp;
                }
            }
        }
        return listofShows;
    }

    public Show[] sortTitleAscending(Show[] show) {
        for (int i = 0; i < show.length; i++) {
            for (int j = i + 1; j < show.length; j++) {
                if (show[i].getTitle().compareToIgnoreCase(show[j].getTitle()) > 0) {
                    Show temp = show[i];
                    show[i] = show[j];
                    show[j] = temp;
                }
            }
        }
        return show;
    }

    public Show[] sortTitleDescending(Show[] show) {
        for (int i = 0; i < show.length; i++) {
            for (int j = i + 1; j < show.length; j++) {
                if (show[i].getTitle().compareToIgnoreCase(show[j].getTitle()) < 0) {
                    Show temp = show[i];
                    show[i] = show[j];
                    show[j] = temp;
                }
            }
        }
        return show;
    }

    public Show[] sortByReleaseYearAsc(Show[] listofShows) {
        for (int i = 0; i < listofShows.length; i++) {
            for (int j = i + 1; j < listofShows.length; j++) {
                if (listofShows[i].getReleaseYear() > (listofShows[j].getReleaseYear())) {
                    Show temp = listofShows[i];
                    listofShows[i] = listofShows[j];
                    listofShows[j] = temp;
                }
            }
        }
        return listofShows;
    }
    public Show[] sortByDurationAscending(Show[] listofShows){
        for (int i = 0; i < listofShows.length; i++) {
            for (int j = i + 1; j < listofShows.length; j++) {
                if (listofShows[i].getDuration() > (listofShows[j].getDuration())) {
                    Show temp = listofShows[i];
                    listofShows[i] = listofShows[j];
                    listofShows[j] = temp;
                }
            }
        }
        return listofShows;
    }
    public Show[] sortByDurationDescending(Show[] listofShows){
        for (int i = 0; i < listofShows.length; i++) {
            for (int j = i + 1; j < listofShows.length; j++) {
                if (listofShows[i].getDuration() < (listofShows[j].getDuration())) {
                    Show temp = listofShows[i];
                    listofShows[i] = listofShows[j];
                    listofShows[j] = temp;
                }
            }
        }
        return listofShows;
    }

    public Show findByTitle(String title) {
        return showsList.stream().filter(show -> show.title.equalsIgnoreCase(title)).findFirst().orElse(null);
    }

    public JTable createTable(Object[][] data, String[] columnNames) {
        return new JTable(new AbstractTableModel() {
            public int getRowCount() {
                return data.length;
            }

            public int getColumnCount() {
                return columnNames.length;
            }

            public String getColumnName(int col) {
                return columnNames[col];
            }

            public Object getValueAt(int rowIndex, int columnIndex) {
                return data[rowIndex][columnIndex];
            }

            public Class getColumnClass(int c) {
                return getValueAt(0, c).getClass();
            }


        }) {
            public boolean editCellAt(int row, int column, EventObject e) {
                return false;
            }
        };
    }

    class HintTextField extends JTextField implements FocusListener {

        private final String hint;
        private boolean showingHint;

        public HintTextField(final String hint) {
            super(hint);
            this.hint = hint;
            this.showingHint = true;
            super.addFocusListener(this);
        }

        public void focusGained(FocusEvent e) {
            if (this.getText().isEmpty()) {
                super.setForeground(Color.BLACK);
                super.setText("");
                showingHint = false;
            }
        }

        public void focusLost(FocusEvent e) {
            if (this.getText().isEmpty()) {
                super.setForeground(Color.GRAY);
                super.setText(hint);
                showingHint = true;
            }
        }

        public String getText() {
            return showingHint ? "" : super.getText();
        }
    }

    /**
     * Gets the values from a TableModel and converts it into a Citizen array
     */
    public Show[] getTableModelValues(TableModel model) {
        Object[][] data = new Object[model.getRowCount()][model.getColumnCount() + 1];
        for (int i = 0; i < model.getRowCount(); i++) {
            data[i][0] = model.getValueAt(i, 0);
            data[i][1] = model.getValueAt(i, 1);
            int duration = Integer.parseInt(model.getValueAt(i, 2).toString().split(" ")[0]);
            String durationType = model.getValueAt(i, 2).toString().split(" ")[1];
            data[i][2] = duration;
            data[i][3] = durationType;
            data[i][4] = model.getValueAt(i, 3);
        }
        return convertToShowsArray(data);
    } // End of getTableModelValues

    /**
     * Converts an Object array to a Show array and returns the Show array
     */
    public Show[] convertToShowsArray(Object[][] data) {
        int length = data.length;
        Show[] shows = new Show[length];
        for (int i = 0; i < length; i++) {
            shows[i] = new Show(findByTitle(data[i][0].toString()));
        }
        return shows;
    } // End of convertToShowsArray

    /**
     * Handles the sorting of values in the JTable
     */
    class TableHeaderMouseListener extends MouseAdapter {
        int sortedColumnNumber = -1;
        boolean ascending = false;

        public void mouseClicked(MouseEvent event) {
            Point point = event.getPoint();
            int columnNumber = table.columnAtPoint(point);
            String[] columnNames = Main.this.columnNames.clone();
            Show[] shows = getTableModelValues(Main.this.table.getModel());
            if (columnNumber == 0) {
                Object[][] sorted;
                if (!ascending) {
                    // Sorts in ascending order
                    columnNames[columnNumber] += " " + '\u25B2';
                    sorted = convertToObjectArray(sortTitleAscending(shows));
                    ascending = true;
                } else {
                    // Sorts in descending order
                    columnNames[columnNumber] += " " + '\u25BC';
                    sorted = convertToObjectArray(sortTitleDescending(shows));
                    ascending = false;

                }

                table.setModel(createTable(sorted, columnNames).getModel());
            }

        } // End of mouseClicked
    } // End of TableHeaderMouseListener class

    class FilterHandler implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            filteredShows = showsArray.clone();
            String title = titleTextField.getText();
            if (!title.equals("Search Title")) {
                filteredShows = filterByTitle(filteredShows, title);
            }
            String genre = genreComboBox.getSelectedIndex() == 0 ? "" : genreComboBox.getSelectedItem().toString();
            filteredShows = filterByGenre(filteredShows, genre);

            int releaseYear = releaseComboBox.getSelectedIndex() == 0 ? 0 : Integer.parseInt(releaseComboBox.getSelectedItem().toString());
            filteredShows = filterByReleaseYear(filteredShows, releaseYear);

            String rating = ratingComboBox.getSelectedIndex() == 0 ? "" : ratingComboBox.getSelectedItem().toString();
            filteredShows = filterByRating(filteredShows, rating);

            int duration = durationComboBox.getSelectedIndex();
            filteredShows = filterByDuration(filteredShows, duration);

            String type = typeComboBox.getSelectedIndex() == 0 ? "" : typeComboBox.getSelectedItem().toString();
            filteredShows = filterByType(filteredShows, type);

            table.setModel(createTable(convertToObjectArray(filteredShows), columnNames).getModel());
        }
    }
}

