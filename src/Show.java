import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Show {
    String title;
    String director;
    String type;
    List<String> cast;
    List<String> country;
    String dateAdded;
    int releaseYear;
    String rating;
    int duration;
    String durationType;
    List<String> genres;

    public Show(String title, String director, String type, List<String> cast, List<String> country, String dateAdded, int releaseYear, String rating, int duration, String durationType, List<String> genres) {
        this.title = title;
        this.director = director;
        this.type = type;
        this.cast = cast;
        this.country = country;
        this.dateAdded = dateAdded;
        this.releaseYear = releaseYear;
        this.rating = rating;
        this.duration = duration;
        this.durationType = durationType;
        this.genres = genres;
    }

    public Show(Show show) {
        this.title = show.title;
        this.director = show.director;
        this.type = show.type;
        this.cast = show.cast;
        this.country = show.country;
        this.dateAdded = show.dateAdded;
        this.releaseYear = show.releaseYear;
        this.rating = show.rating;
        this.duration = show.duration;
        this.durationType = show.durationType;
        this.genres = show.genres;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<String> genres) {
        this.genres = genres;
    }


    public String getTitle() {
        return this.title;
    }

    public void showFrame() {
        JFrame frame = new JFrame(title);
        JPanel panel = new JPanel(), titlePanel = new JPanel(), detailsPanel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setPreferredSize(new Dimension(980, 200));
        JLabel titleLabel, typeLabel, releaseYearLabel, ratingLabel, durationLabel;
        JTextArea extendedDetailsTextArea;

        Font normalFont = new Font("Comic Sans", Font.PLAIN, 12);

        titleLabel = new JLabel(title);
        titleLabel.setFont(new Font("Comic Sans", Font.BOLD, 28));
        titlePanel.add(titleLabel);
        titlePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        typeLabel = new JLabel(type);
        typeLabel.setFont(normalFont);
        releaseYearLabel = new JLabel(String.valueOf(releaseYear));
        releaseYearLabel.setFont(normalFont);
        durationLabel = new JLabel(duration + " " + durationType);
        durationLabel.setFont(normalFont);
        ratingLabel = new JLabel(rating);
        ratingLabel.setFont(normalFont);

        extendedDetailsTextArea = new JTextArea("Cast: " + cast.toString().replace("[", "").replace("]", ""));
        extendedDetailsTextArea.setText(extendedDetailsTextArea.getText() + "\nDirected by: " + director);
        extendedDetailsTextArea.setText(extendedDetailsTextArea.getText() + "\nGenre/s: " + genres.toString().replace("[", "").replace("]", ""));
        extendedDetailsTextArea.setText(extendedDetailsTextArea.getText() + "\nCountry/ies: " + country.toString().replace("[", "").replace("]", ""));
        extendedDetailsTextArea.setText(extendedDetailsTextArea.getText() + "\nDate added to Netflix: " + dateAdded);
        extendedDetailsTextArea.setWrapStyleWord(true);
        extendedDetailsTextArea.setLineWrap(true);
        extendedDetailsTextArea.setEditable(false);
        extendedDetailsTextArea.setFocusable(false);
        extendedDetailsTextArea.setBackground(UIManager.getColor("Label.background"));
        extendedDetailsTextArea.setFont(normalFont);
        extendedDetailsTextArea.setBorder(null);
        extendedDetailsTextArea.setAlignmentX(Component.LEFT_ALIGNMENT);

        detailsPanel.add(releaseYearLabel);
        detailsPanel.add(ratingLabel);
        detailsPanel.add(durationLabel);
        detailsPanel.add(typeLabel);
        detailsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        panel.add(titlePanel);
        panel.add(detailsPanel);
        panel.add(extendedDetailsTextArea);

        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        frame.getContentPane().add(scrollPane);
        frame.pack();
        frame.setResizable(true);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getDuration() { return this.duration; }

}
